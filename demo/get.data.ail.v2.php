<?php
/**
 * Kuerzi Planungstool, v01
 * 
 * Resources: 
 *  - MSSQL
 */

//-----------------------------------------------------
// get credentials
//-----------------------------------------------------  
include( 'config.php' );

//-----------------------------------------------------
// get cost per hour per division
//-----------------------------------------------------
$sqlHourRate = 'SELECT * FROM vw_mat_preise';
$stmtHourlyRate = $conn->prepare( $sqlHourRate );
$stmtHourlyRate->execute();
$result = $stmtHourlyRate->fetch(PDO::FETCH_ASSOC);
 
// cost per hour per division
$costPerHour_DO = $result['DO'];
$costPerHour_PO = $result['PO'];
$costPerHour_MO = $result['MO'];


//-----------------------------------------------------
// query orders
//-----------------------------------------------------
$sql = "SELECT * FROM vw_gantt_auftraege_2";

$stmt = $conn->prepare( $sql );

if ( $stmt->execute() ) {

// collect result
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

$i = 0;
$idPro = 0;
$dataset = array();
$dsAil   = array();


//-----------------------------------------------------
// loop results from vw_gantt_auftraege_2 (orders)
//-----------------------------------------------------
foreach ( $result as $row ) {
    
if ( $idPro != $row['PRO_ID'] ){
    
/*
    $sqlSum = "SELECT PRO_ID, sum(DO_MAX_Cost) AS SUM_DO, sum(MO_MAX_Cost) AS SUM_MO, sum(PO_MAX_Cost) AS SUM_PO, sum(MAT_MAX_Cost) AS SUM_Mat
                FROM vw_gantt_auftraege_2
                WHERE PRO_ID=".$row['PRO_ID'] ."
                GROUP BY PRO_ID ";
                
    $stmtSum = $conn->prepare( $sqlSum );
    $stmtSum->execute();
    $resultSum = $stmtSum->fetch(PDO::FETCH_ASSOC);
    
    // calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
    $sum_DO = round( $resultSum['SUM_DO'] / $costPerHour_DO );
    $sum_PO = round( $resultSum['SUM_PO'] / $costPerHour_PO );
    $sum_MO = round( $resultSum['SUM_MO'] / $costPerHour_MO );
    
    
  
  //if ( $k == 'PRO_ID' ){

    $dataset[] = array(
      
      'id'        => $row['PRO_ID'],
      'parent'    => 0,
      'start_date' => "",
      'text'      => utf8_encode( $row['PRO_Bezeichnung'] ),
      'progress'  => 1,
      'duration'  => "",
      'sortorder' => ++$i,
      'open'      => false,
      'DO_MAX_Cost'    => $sum_DO .'/'. $resultSum['SUM_DO'],
      'PO_MAX_Cost'    => $sum_PO .'/'. $resultSum['SUM_PO'],
      'MO_MAX_Cost'    => $sum_MO .'/'. $resultSum['SUM_MO'],
      'MAT_MAX_Cost'    => $resultSum['SUM_Mat'],
      'type'      => 'project'

    );
*/

    $sqlSum = "SELECT
	sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_PO_Effort,
	sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_DO_Effort,
	sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_MO_Effort,
	sum(DO_MAX_Cost) AS SUM_DO,
	sum(MO_MAX_Cost) AS SUM_MO,
	sum(PO_MAX_Cost) AS SUM_PO,
	sum(MAT_MAX_Cost) AS SUM_Mat,	
	RKA_ID,
	PRO_ID

                FROM vw_gantt_auftraege_3
                WHERE PRO_ID=".$row['PRO_ID'] ."
                GROUP BY PRO_ID, RKA_ID
    ";
                
    $stmtSum = $conn->prepare( $sqlSum );
    $stmtSum->execute();
    $resultSum = $stmtSum->fetch(PDO::FETCH_ASSOC);
    
    // calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
    $sum_DO = round( $resultSum['SUM_DO'] / $costPerHour_DO );
    $sum_PO = round( $resultSum['SUM_PO'] / $costPerHour_PO );
    $sum_MO = round( $resultSum['SUM_MO'] / $costPerHour_MO );
    $sum_MAT = $resultSum['SUM_Mat'] != '' ? $resultSum['SUM_Mat'] : 0;
    
    $sum_effort_DO = !empty( $resultSum['SUM_DO_Effort'] ) ? $resultSum['SUM_DO_Effort'] : 0;
    $sum_effort_PO = $resultSum['SUM_PO_Effort'] != '' ? $resultSum['SUM_PO_Effort'] : 0;
    $sum_effort_MO = $resultSum['SUM_MO_Effort'] != '' ? $resultSum['SUM_MO_Effort'] : 0;
    

    // populate array
    $dataset[] = array(
      
      'id'        => $row['PRO_ID'],
      'parent'    => 0,
      'start_date' => "",
      'text'      => $row['PRO_Bezeichnung'] .'_'. $row['PRO_ID'],
      'progress'  => 1,
      'duration'  => "",
      'sortorder' => ++$i,
      'open'      => false,
      'DO_MAX_Cost'    => $sum_effort_DO .'/'. $sum_DO,
      'PO_MAX_Cost'    => $sum_effort_PO .'/'. $sum_PO,
      'MO_MAX_Cost'    => $sum_effort_MO .'/'. $sum_MO,
      'MAT_MAX_Cost'   => $sum_MAT,
      'type'      => 'project'

    );
  
}         

$hourdiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/3600, 1);
$daydiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/86400, 1);

$RKA_ID = $row['RKA_ID'];

// calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
$do_max_cost = isset( $row['DO_MAX_Cost'] ) ? round( $row['DO_MAX_Cost'] / $costPerHour_DO ) : 0;
$po_max_cost = isset( $row['PO_MAX_Cost'] ) ? round( $row['PO_MAX_Cost'] / $costPerHour_PO ) : 0;
$mo_max_cost = isset( $row['MO_MAX_Cost'] ) ? round( $row['MO_MAX_Cost'] / $costPerHour_MO ) : 0;

$mat_max_cost = isset( $row['MAT_MAX_Cost'] ) ?  $row['MAT_MAX_Cost'] : '';

$UserId_ = isset( $row['UserId'] ) && is_int( $row['UserId'] ) ? $row['UserId'] : 0;
  
  $dataset[] = array(
      'id'          => $RKA_ID,
      'parent'      => $row['PRO_ID'],
      'text'        => $row['RKA_Bezeichnung'],
      'owner_id'    => $UserId_,
      'start_date'  => $row['startAuftragDatum'],
      'progress'    => 1,
      'duration'    => $daydiff,
      'sortorder'   => ++$i,
      'open'        => false,
      'DO_MAX_Cost' => $do_max_cost,
      'PO_MAX_Cost' => $po_max_cost,
      'MO_MAX_Cost' => $mo_max_cost,
      'MAT_MAX_Cost' => $mat_max_cost,
      'type'        => 'task'
    );
    
  
  
  $sqlAil = "SELECT
              RKA_ID,
              ARK_ID,
              Action_Item_Title,
              Action_Item_date,
              Action_Item_Target_date,
              Action_Item_Effort
            FROM
              A_R_KAUFTRAG
            WHERE RKA_ID=".$RKA_ID; 
            
 $stmtAil = $conn->prepare( $sqlAil );
 

 if (  $stmtAil->execute() ) {
   
    
  $resultAil = $stmtAil->fetchAll(PDO::FETCH_ASSOC);
  
  foreach ( $resultAil as $row2 ) {
    
    $daydiffAil = round((strtotime($row2['Action_Item_Target_date']) - strtotime($row2['Action_Item_date']))/86400, 1);
    
    
    $dataset[] = array(
      'id' => $row2['ARK_ID'],
      'parent' => $RKA_ID,
      'text' =>  utf8_encode( $row2['Action_Item_Title'] ),
      'start_date' => $row2['Action_Item_date'],
      'progress'  => $row2['Action_Item_Effort'],
      'duration'  => $daydiffAil,
      'sortorder' => ++$i,
      'open'      => false,
      'DO_MAX_Cost'    => 0,
      'PO_MAX_Cost'    => 0,
      'MO_MAX_Cost'    => 0,
      'MAT_MAX_Cost'    => 0,
      'type'      => 'subtask',
      'color' => '#c30'
    );
    
  }

  //} // rowCount
  
  }

$idPro = $row['PRO_ID'];
//++$i;
  
} // end foreach
    
    //-----------------------------------------------------
    // echo result as json
    //-----------------------------------------------------
    $json = json_encode(  array( 'data' => $dataset ) ); 
    echo ( $json );

} // if execute
else {
    //-----------------------------------------------------
    // if query fails, echo error
    //-----------------------------------------------------
    echo 'error';
}
  