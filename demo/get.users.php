<?php
/**
 * Kuerzi Planungstool, v01
 * 
 * Resources: 
 *  - MSSQL
 */

//-----------------------------------------------------
// get credentials
//-----------------------------------------------------  
include( 'config.php' );

//-----------------------------------------------------
// get cost per hour per division
//-----------------------------------------------------
$sqlUsers = 'SELECT BOUserId as id, UserName as text FROM aspnet_Users WHERE BOUserId IS NOT null';
$stmtUsers = $conn->prepare( $sqlUsers );
$stmtUsers->execute();
$result = $stmtUsers->fetchAll( PDO::FETCH_ASSOC );


//echo '<pre>';
//echo json_encode( $result );
//echo '</pre>';

$users = array();
$i = 0;

foreach( $result as $user ){
    
    $users[$i]['id'] = $user['id']; 
    $users[$i]['text'] = $user['text']; 
    $users[$i]['parent'] = null; 
    
    $i++;
}

echo json_encode( $users );