<?php
/**
 * Kuerzi Planungstool, v01
 * 
 * Resources: 
 *  - MSSQL
 */

//-----------------------------------------------------
// get credentials
//-----------------------------------------------------  
include( 'config.php' );

//-----------------------------------------------------
// get cost per hour per division
//-----------------------------------------------------
$sqlHourRate = 'SELECT * FROM vw_mat_preise';
$stmtHourlyRate = $conn->prepare( $sqlHourRate );
$stmtHourlyRate->execute();
$result = $stmtHourlyRate->fetch(PDO::FETCH_ASSOC);
 
// cost per hour per division
$costPerHour_DO = $result['DO'];
$costPerHour_PO = $result['PO'];
$costPerHour_MO = $result['MO'];


//-----------------------------------------------------
// query orders
//-----------------------------------------------------
// $sql = "SELECT * FROM vw_gantt_auftraege_owner WHERE PRO_ID=618  --( endeAuftragDatum>GETDATE() OR endeAuftragDatum IS NULL ) AND ( startAuftragDatum IS NOT NULL )'";
$sql = "SELECT * FROM vw_gantt_projekte --WHERE PRO_ID=1620";

// db object
$stmt = $conn->prepare( $sql );

if ( $stmt->execute() ) {

// collect result
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// init vars
$i = 0;
$idPro = 0;
$dataset = array();
$dsAil   = array();

$sum_effort_DO = 0;
$sum_effort_PO = 0;
$sum_effort_MO = 0;


//-----------------------------------------------------
// loop results from vw_gantt_auftraege_2 (orders)
//-----------------------------------------------------
foreach ( $result as $row ) {
    
if ( $idPro != $row['PRO_ID'] ){
    
    // db query
/*
    $sqlSum = "SELECT PRO_ID, sum(DO_MAX_Cost) AS SUM_DO, sum(MO_MAX_Cost) AS SUM_MO, sum(PO_MAX_Cost) AS SUM_PO, sum(MAT_MAX_Cost) AS SUM_Mat
                FROM vw_gantt_auftraege_3
                WHERE PRO_ID=".$row['PRO_ID'] ."
                GROUP BY PRO_ID";
                
                
                
    SUM Effort per cost center
    //=============================
    SELECT
    	sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_PO,
    	sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_DO,
    	sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_MO,
    	RKA_ID,
    	PRO_ID

    FROM vw_gantt_auftraege_3
        WHERE PRO_ID=1620
        GROUP BY PRO_ID, RKA_ID
*/
    
    /*
        Sum offered costs of DO, PO, MO and MAT, divide them by the price per hour (get from DB).
        In addition get the sum of work hours (effort) per cost center (MO, PO, DO)
        
    */
    
    
    // calc sum of effort per cost center on project
    //------------------------------------------------------------------------------------------------------------
    $sqlEffortSum = "
    SELECT
    	sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_PO,
    	sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_DO,
    	sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_MO,
    	PRO_ID

    FROM vw_gantt_auftraege_3
        WHERE PRO_ID=".$row['PRO_ID'] ."
        GROUP BY PRO_ID
    ";
    
    $stmtSumEffort = $conn->prepare( $sqlEffortSum );
    $stmtSumEffort->execute();
    $resultSumEffort = $stmtSumEffort->fetch(PDO::FETCH_ASSOC);
    
    
    $sum_effort_PO = $resultSumEffort['SUM_Effort_PO'];
    $sum_effort_DO = $resultSumEffort['SUM_Effort_DO'];
    $sum_effort_MO = $resultSumEffort['SUM_Effort_MO'];
    
           
    $sqlSum = "SELECT
	--sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_PO_Effort,
	--sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_DO_Effort,
	--sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_MO_Effort,
	sum(DO_MAX_Cost) AS SUM_DO,
	sum(MO_MAX_Cost) AS SUM_MO,
	sum(PO_MAX_Cost) AS SUM_PO,
	sum(MAT_MAX_Cost) AS SUM_Mat,	
	RKA_ID,
	PRO_ID

                FROM vw_gantt_auftraege_2
                WHERE PRO_ID=".$row['PRO_ID'] ."
                GROUP BY PRO_ID, RKA_ID
    ";
                
    $stmtSum = $conn->prepare( $sqlSum );
    $stmtSum->execute();
    $resultSum = $stmtSum->fetch(PDO::FETCH_ASSOC);
    
    // calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
    $sum_DO = round( $resultSum['SUM_DO'] / $costPerHour_DO );
    $sum_PO = round( $resultSum['SUM_PO'] / $costPerHour_PO );
    $sum_MO = round( $resultSum['SUM_MO'] / $costPerHour_MO );
    $sum_MAT = $resultSum['SUM_Mat'] != '' ? $resultSum['SUM_Mat'] : 0;
    
    /*
    $sum_effort_DO = !empty( $resultSum['SUM_DO_Effort'] ) ? $resultSum['SUM_DO_Effort'] : 0;
    $sum_effort_PO = $resultSum['SUM_PO_Effort'] != '' ? $resultSum['SUM_PO_Effort'] : 0;
    $sum_effort_MO = $resultSum['SUM_MO_Effort'] != '' ? $resultSum['SUM_MO_Effort'] : 0;
    */
    

    // populate array
    $dataset[] = array(
      
      'id'        => $row['PRO_ID'],
      'parent'    => 0,
      'start_date' => "",
      'text'      => $row['PRO_Bezeichnung'] .' -> '. $row['PRO_ID'],
      'progress'  => 1,
      'duration'  => "",
      'sortorder' => ++$i,
      'open'      => false,
      'DO_MAX_Cost'    => $sum_effort_DO .'/'. $sum_DO,
      'PO_MAX_Cost'    => $sum_effort_PO .'/'. $sum_PO,
      'MO_MAX_Cost'    => $sum_effort_MO .'/'. $sum_MO,
      'MAT_MAX_Cost'   => $sum_MAT,
      'type'      => 'project'

    );
  
}   



// calc sum of effort per cost center on project
//------------------------------------------------------------------------------------------------------------
$sqlEffortSum = "
SELECT
	sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_PO,
	sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_DO,
	sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_MO,
	RKA_ID,
	PRO_ID

FROM vw_gantt_auftraege_3
    WHERE RKA_ID=".$row['RKA_ID'] ."
    GROUP BY PRO_ID, RKA_ID
";

$stmtSumEffort = $conn->prepare( $sqlEffortSum );
$stmtSumEffort->execute();
$resultSumEffort = $stmtSumEffort->fetch(PDO::FETCH_ASSOC);


$sum_effort_PO = $resultSumEffort['SUM_Effort_PO'];
$sum_effort_DO = $resultSumEffort['SUM_Effort_DO'];
$sum_effort_MO = $resultSumEffort['SUM_Effort_MO'];

$hourdiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/3600, 1);
$daydiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/86400, 1);

// prevent negative numbers
//$daydiff = abs( $daydiff );

$RKA_ID = $row['RKA_ID'];

// calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
$do_max_cost = isset( $row['DO_MAX_Cost'] ) ? round( $row['DO_MAX_Cost'] / $costPerHour_DO ) : '';
$po_max_cost = isset( $row['PO_MAX_Cost'] ) ? round( $row['PO_MAX_Cost'] / $costPerHour_PO ) : '';
$mo_max_cost = isset( $row['MO_MAX_Cost'] ) ? round( $row['MO_MAX_Cost'] / $costPerHour_MO ) : '';

$mat_max_cost = isset( $row['MAT_MAX_Cost'] ) ?  $row['MAT_MAX_Cost'] : '';

$UserId_ = isset( $row['UserId'] ) && is_int( $row['UserId'] ) ? $row['UserId'] : 0;
  
  $dataset[] = array(
      'id'          => $RKA_ID,
      'parent'      => $row['PRO_ID'],
      'text'        => $row['RKA_Bezeichnung'] . ' -> '. $RKA_ID,
      'owner_id'    => $UserId_,
      'start_date'  => $row['startAuftragDatum'],
      'progress'    => 1,
      'duration'    => $daydiff,
      'sortorder'   => ++$i,
      'open'        => false,
      'DO_MAX_Cost' => $sum_effort_DO,
      'PO_MAX_Cost' => $sum_effort_PO,
      'MO_MAX_Cost' => $sum_effort_MO,
      'MAT_MAX_Cost' => $mat_max_cost,
      'type'        => 'task'
    );
    
  
  
  $sqlAil = "SELECT
                [A_R_KAUFTRAG].[RKA_ID],
                [A_R_KAUFTRAG].[ARK_ID],
                [A_R_KAUFTRAG].[Action_Item_Title],
                [dbo].[aspnet_Users].[BOUserId] as UserId,
                [dbo].[aspnet_Users].[UserName],
                [A_R_KAUFTRAG].[Action_Item_date],
                [A_R_KAUFTRAG].[Action_Item_Target_date],
                [A_R_KAUFTRAG].[Action_Item_Effort],
                [dbo].[E_R_KAUFTRAG].[DO_MAX_Cost],
                [dbo].[E_R_KAUFTRAG].[PO_MAX_Cost],
                [dbo].[E_R_KAUFTRAG].[MO_MAX_Cost],
                [dbo].[E_R_KAUFTRAG].[MAT_MAX_Cost],
                [dbo].[E_R_KAUFTRAG].[EDL_MAX_Cost]
                FROM
                [A_R_KAUFTRAG]
                JOIN [dbo].[E_R_KAUFTRAG]
                ON [A_R_KAUFTRAG].[RKA_ID] = [dbo].[E_R_KAUFTRAG].[RK_ID]
                LEFT JOIN [dbo].[aspnet_Users]
                ON [dbo].[A_R_KAUFTRAG].[Action_Item_Resp] LIKE [dbo].[aspnet_Users].[UserName]
                WHERE
                RKA_ID=".$RKA_ID; 
            
 $stmtAil = $conn->prepare( $sqlAil );
 

 if (  $stmtAil->execute() ) {
   
    
  $resultAil = $stmtAil->fetchAll(PDO::FETCH_ASSOC);
  
  foreach ( $resultAil as $row2 ) {
      
      
    // calc sum of effort per cost center on project
    //------------------------------------------------------------------------------------------------------------
    $sqlEffortSum = "
    SELECT
    	sum(CASE WHEN Action_Item_Department LIKE 'PO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_PO,
    	sum(CASE WHEN Action_Item_Department LIKE 'DO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_DO,
    	sum(CASE WHEN Action_Item_Department LIKE 'MO' THEN Action_Item_Effort ELSE 0 END) as SUM_Effort_MO,
    	RKA_ID,
    	PRO_ID

    FROM vw_gantt_auftraege_3
        WHERE ARK_ID=".$row2['ARK_ID'] ."
        GROUP BY PRO_ID, RKA_ID
    ";
    
    $stmtSumEffort = $conn->prepare( $sqlEffortSum );
    $stmtSumEffort->execute();
    $resultSumEffort = $stmtSumEffort->fetch(PDO::FETCH_ASSOC);
    
    
    $sum_effort_PO = isset( $resultSumEffort['SUM_Effort_PO'] ) ? $resultSumEffort['SUM_Effort_PO'] : 0;
    $sum_effort_DO = isset( $resultSumEffort['SUM_Effort_DO'] ) ? $resultSumEffort['SUM_Effort_DO'] : 0;
    $sum_effort_MO = isset( $resultSumEffort['SUM_Effort_MO'] ) ? $resultSumEffort['SUM_Effort_MO'] : 0;  
    
    
    $daydiffAil = round((strtotime($row2['Action_Item_Target_date']) - strtotime($row2['Action_Item_date']))/86400, 1);
    
    // prevent negative numbers
    //$$daydiffAil = abs( $daydiffAil );
    
    // calculate hours from offered price by dividing in price per hour retrieved from M_MATERIALPREISE
    $sum_max_DO = round( $row2['DO_MAX_Cost'] / $costPerHour_DO );
    $sum_max_PO = round( $row2['PO_MAX_Cost'] / $costPerHour_PO );
    $sum_max_MO = round( $row2['MO_MAX_Cost'] / $costPerHour_MO );
    
    $sum_max_DO = '';
    $sum_max_PO = '';
    $sum_max_MO = '';
    
    $UserId = is_int( $row2['UserId'] ) ? $row2['UserId'] : 0;
    
    
    $dataset[] = array(
      'id' => $row2['ARK_ID'],
      'parent' => $RKA_ID,
      'owner_id' => $UserId,
      'text' =>  $row2['Action_Item_Title'],
      'start_date' => $row2['Action_Item_date'],
      //'progress'  => $row2['Action_Item_Effort'],
      'progress'  => 0,
      'duration'  => $daydiffAil,
      'sortorder' => ++$i,
      'open'      => false,
      'DO_MAX_Cost'    => $sum_effort_DO,
      'PO_MAX_Cost'    => $sum_effort_PO,
      'MO_MAX_Cost'    => $sum_effort_MO,
      'MAT_MAX_Cost'    => $row2['MAT_MAX_Cost'],
      'type'      => 'subtask',
      'color' => '#c30'
    );
    
  }

  //} // rowCount
  
  }

$idPro = $row['PRO_ID'];
//++$i;
  
} // end foreach
    
    //-----------------------------------------------------
    // echo result as json
    //-----------------------------------------------------
    $json = json_encode(  array( 'data' => $dataset ) ); 
    
    $fp = fopen('data.json', 'w');
    fwrite($fp, $json );
    fclose($fp);
    
    echo ( $json );

} // if execute
else {
    //-----------------------------------------------------
    // if query fails, echo error
    //-----------------------------------------------------
    echo 'error';
}
  