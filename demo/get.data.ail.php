<?php
  
  include( 'config.php' );
    
  
  $dsn = "dblib:host=".DBHOST.";dbname=".DB;
  //$dsn = 'dblib:dbname=E_Kuerzi;host=52.59.227.217';
  //$dsn = "odbc:KuerziAWS;Database=E_Kuerzi";
  $conn = new PDO($dsn, "WIN-MSNERGKR68G\Administrator", "DtSo7a1S!");
  $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  
  
  $sql = "SELECT * FROM vw_gantt_auftraege_2";
  
  $stmt = $conn->prepare( $sql );
  
  if ( $stmt->execute() ) {
    
  
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $i = 0;
  $idPro = 0;
  $dataset = array();
  $dsAil   = array();
  
  foreach ( $result as $row ) {
        
    if ( $idPro != $row['PRO_ID'] ){
        
        $sqlSum = "SELECT PRO_ID, sum(DO_MAX_Cost) AS SUM_DO, sum(MO_MAX_Cost) AS SUM_MO, sum(PO_MAX_Cost) AS SUM_PO, sum(MAT_MAX_Cost) AS SUM_Mat
                    FROM vw_gantt_auftraege_2
                    WHERE PRO_ID=".$row['PRO_ID'] ."
                    GROUP BY PRO_ID ";
                    
        $stmtSum = $conn->prepare( $sqlSum );
        $stmtSum->execute();
        $resultSum = $stmtSum->fetch(PDO::FETCH_ASSOC);
      
      //if ( $k == 'PRO_ID' ){
    
        $dataset[] = array(
          
          'id'        => $row['PRO_ID'],
          'parent'    => 0,
          'start_date' => "",
          'text'      => utf8_encode( $row['PRO_Bezeichnung'] ),
          'progress'  => 1,
          'duration'  => "",
          'sortorder' => ++$i,
          'open'      => false,
          'DO_MAX_Cost'    => $resultSum['SUM_DO'],
          'PO_MAX_Cost'    => $resultSum['SUM_PO'],
          'MO_MAX_Cost'    => $resultSum['SUM_MO'],
          'MAT_MAX_Cost'    => $resultSum['SUM_Mat'],
          'type'      => 'project'

        );
      
    }         
    
    $hourdiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/3600, 1);
    $daydiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/86400, 1);
    
    $RKA_ID = $row['RKA_ID'];
      
      $dataset[] = array(
          'id' => $RKA_ID,
          'parent' => $row['PRO_ID'],
          'text' => utf8_encode( $row['RKA_Bezeichnung'] ),
          'start_date' => $row['startAuftragDatum'],
          'progress'  => 1,
          'duration'  => $daydiff,
          'sortorder' => ++$i,
          'open'      => false,
          'DO_MAX_Cost'    => $row['DO_MAX_Cost'],
          'PO_MAX_Cost'    => $row['PO_MAX_Cost'],
          'MO_MAX_Cost'    => $row['MO_MAX_Cost'],
          'MAT_MAX_Cost'    => $row['MAT_MAX_Cost'],
          'type'      => 'task'
        );
        
      
      
      $sqlAil = "SELECT
                  RKA_ID,
                  ARK_ID,
                  Action_Item_Title,
                  Action_Item_date,
                  Action_Item_Target_date,
                  Action_Item_Effort
                FROM
                  A_R_KAUFTRAG
                WHERE RKA_ID=".$RKA_ID; 
                
     $stmtAil = $conn->prepare( $sqlAil );
     
  
     if (  $stmtAil->execute() ) {
       
        
      $resultAil = $stmtAil->fetchAll(PDO::FETCH_ASSOC);
      
      foreach ( $resultAil as $row2 ) {
        
        $daydiffAil = round((strtotime($row2['Action_Item_Target_date']) - strtotime($row2['Action_Item_date']))/86400, 1);
        
        
        $dataset[] = array(
          'id' => $row2['ARK_ID'],
          'parent' => $RKA_ID,
          'text' =>  utf8_encode( $row2['Action_Item_Title'] ),
          'start_date' => $row2['Action_Item_date'],
          'progress'  => $row2['Action_Item_Effort'],
          'duration'  => $daydiffAil,
          'sortorder' => ++$i,
          'open'      => false,
          'DO_MAX_Cost'    => '',
          'PO_MAX_Cost'    => '',
          'MO_MAX_Cost'    => '',
          'MAT_MAX_Cost'    => '',
          'type'      => 'subtask',
          'color' => '#c30'
        );
        
      }

      //} // rowCount
      
      }
    
    $idPro = $row['PRO_ID'];
    //++$i;
      
  } 
  
  $json = json_encode(  array( 'data' => $dataset ) ); 
  echo ( $json );
  
  } // if execute
  else {
    
    echo 'error';
  }
  