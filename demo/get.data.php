<?php
//-----------------------------------------------------
// get credentials
//-----------------------------------------------------  
include( 'config.php' );
  
  
  $sql = "SELECT * FROM vw_gantt_auftraege_2";
  
  $stmt = $conn->prepare( $sql );
  
  if ( $stmt->execute() ) {
    
  
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $i = 0;
  $idPro = 0;
  $dataset = array();
  
  foreach ( $result as $row ) {
        
    if ( $idPro != $row['PRO_ID'] ){
      
      
      //if ( $k == 'PRO_ID' ){
    
        $dataset[] = array(
          
          'id'        => $row['PRO_ID'],
          'parent'    => 0,
          'start_date' => "",
          'text'      => utf8_encode( $row['PRO_Bezeichnung'] ),
          'progress'  => 1,
          'duration'  => "",
          'sortorder' => ++$i,
          'open'      => 1,
          'DO_MAX_Cost'    => '',
          'PO_MAX_Cost'    => '',
          'MO_MAX_Cost'    => '',
          'MAT_MAX_Cost'    => '',
          'type'      => 'project'

        );
      
    }         
    
    $hourdiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/3600, 1);
    $daydiff = round((strtotime($row['endeAuftragDatum']) - strtotime($row['startAuftragDatum']))/86400, 1);
      
      
      $dataset[] = array(
          'id' => $row['RKA_ID'],
          'parent' => $row['PRO_ID'],
          'text' => utf8_encode( $row['RKA_Bezeichnung'] ),
          'start_date' => $row['startAuftragDatum'],
          'progress'  => 1,
          'duration'  => $daydiff,
          'sortorder' => ++$i,
          'open'      => 1,
          'DO_MAX_Cost'    => $row['DO_MAX_Cost'],
          'PO_MAX_Cost'    => $row['PO_MAX_Cost'],
          'MO_MAX_Cost'    => $row['MO_MAX_Cost'],
          'MAT_MAX_Cost'    => $row['PO_MAX_Cost'],
          'type'      => 'task'
          
        );
    
    $idPro = $row['PRO_ID'];
    //++$i;
      
  } 
  
  $json = json_encode(  array( 'data' => $dataset ) ); 
  echo ( $json );
  
  } // if execute
  else {
    
    echo 'error';
  }
  