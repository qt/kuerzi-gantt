SELECT DISTINCT
	Kuerzi.dbo.P_PROJEKT.PRO_ID,
	Kuerzi.dbo.P_PROJEKT.PRO_Bezeichnung,
	Kuerzi.dbo.R_KAUFTRAG.RKA_ID,
	Kuerzi.dbo.R_KAUFTRAG.RKA_Bezeichnung,
	Kuerzi.dbo.R_KAUFTRAG.RKA_Datum2 AS startAuftragDatum,
	Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiDatum1 AS endeAuftragDatum,
	dbo.E_R_KAUFTRAG.DO_MAX_Cost,
	dbo.E_R_KAUFTRAG.MO_MAX_Cost,
	dbo.E_R_KAUFTRAG.PO_MAX_Cost,
	dbo.E_R_KAUFTRAG.MAT_MAX_Cost,
	Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiZahl2 AS inWork
FROM
	Kuerzi.dbo.R_KAUFTRAG
INNER JOIN Kuerzi.dbo.P_PROJEKT ON Kuerzi.dbo.R_KAUFTRAG.RKA_PRO = Kuerzi.dbo.P_PROJEKT.PRO_ID
INNER JOIN dbo.E_R_KAUFTRAG ON Kuerzi.dbo.R_KAUFTRAG.RKA_ID = dbo.E_R_KAUFTRAG.RK_ID
INNER JOIN Kuerzi.dbo.R_TBAUFTRAG ON Kuerzi.dbo.R_KAUFTRAG.RKA_ID = Kuerzi.dbo.R_TBAUFTRAG.RTB_RKA
INNER JOIN dbo.A_R_KAUFTRAG ON Kuerzi.dbo.R_KAUFTRAG.RKA_ID = dbo.A_R_KAUFTRAG.RKA_ID
WHERE
	Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiZahl2 <> 99 --Kuerzi.dbo.R_KAUFTRAG.RKA_Datum2<GETDATE() AND Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiDatum1>GETDATE() 