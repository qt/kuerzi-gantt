SELECT
	Kuerzi.dbo.P_PROJEKT.PRO_ID,
	Kuerzi.dbo.P_PROJEKT.PRO_Bezeichnung,
	Kuerzi.dbo.P_PROJEKT.PRO_Start,
	Kuerzi.dbo.R_KAUFTRAG.RKA_ID,
	Kuerzi.dbo.R_KAUFTRAG.RKA_Bezeichnung,
	Kuerzi.dbo.R_KAUFTRAG.RKA_Datum1 AS startAuftragDatum,
	Kuerzi.dbo.R_KAUFTRAG.RKA_Datum2 AS engAvorDatum,
	Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiDatum1 AS prodDatum,
	Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiDatum2 AS endeAuftragDatum,
	Kuerzi.dbo.R_KAUFTRAG.RKA_ExtAufNr,
	dbo.E_R_KAUFTRAG.RK_ID,
	dbo.E_R_KAUFTRAG.DO_MAX_Cost,
	dbo.E_R_KAUFTRAG.PO_MAX_Cost,
	dbo.E_R_KAUFTRAG.MO_MAX_Cost,
	dbo.E_R_KAUFTRAG.MAT_MAX_Cost
FROM
	Kuerzi.dbo.P_PROJEKT
INNER JOIN Kuerzi.dbo.R_KAUFTRAG ON Kuerzi.dbo.P_PROJEKT.PRO_ID = Kuerzi.dbo.R_KAUFTRAG.RKA_PRO
INNER JOIN Kuerzi.dbo.R_TBAUFTRAG ON Kuerzi.dbo.R_KAUFTRAG.RKA_ID = Kuerzi.dbo.R_TBAUFTRAG.RTB_RKA
INNER JOIN dbo.E_R_KAUFTRAG ON Kuerzi.dbo.R_KAUFTRAG.RKA_ID = dbo.E_R_KAUFTRAG.RK_ID
WHERE
	(
		Kuerzi.dbo.R_TBAUFTRAG.RTB_FreiZahl2 <> 99
	)